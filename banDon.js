const token = "6160690570:AAG0IRPRkizPkw2hgZgp7P3hEnp4Z3Yvcko";
const chatId = "6132800988";
const message = "Hello, Telegram!";

const apiUrl = `https://api.telegram.org/bot${token}/sendMessage`;

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
// btn.onclick = function () {
//   modal.style.display = "block";
// };

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

const postDataToTele = () => {
  const form = document.getElementById("3lz7xo0q");
  const hoVaTen = form["wi-9nawvfzo"].value;
  const soDienThoai = form["wi-f4pk9uwb"].value;
  const diaChi = form["wi-0psglcj4"].value;
  const chiTiet = form["wi-qf0xngyd"].value;
  const soLuong = form["wi-iu44g4ro"].value;

  const msg = `Họ và tên: ${hoVaTen} \nSố điện thoại: ${soDienThoai} \nĐịa chỉ: ${diaChi} \nChi tiết: ${chiTiet} \nSố lượng: ${soLuong}`;

  // const submitForm = document.getElementById("3lz7xo0q");

  form.addEventListener("submit", (e) => {
    e.preventDefault();
    fetch(apiUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        chat_id: chatId,
        // text: JSON.stringify(data),
        text: msg,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.ok) {
          console.log("Message sent successfully");
          modal.style.display = "block";
        } else {
          console.log("Failed to send message:", data.description);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  });
};
